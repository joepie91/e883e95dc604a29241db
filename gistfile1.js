function runBow(input, cb) {
      return Promise.try(function(){
            if (input.url.match(/^https:\/\//i)) {
                  /* Mutating is bad! */
                  var url = input.url.replace(/^https:\/\//i, 'http://');
            } else {
                  var url = input.url;
            }
            
            return bow.crawl(url);
      })
      .map(bow.parseResults, {concurrency: 20})
      .then(function(parsedResults){
            /* parsedResults now has an array of parsed results... */
            console.log("Done!");
      })
      /* and if you want to handle errors here: (but you probably don't need to, as they bubble up) */
      .catch(function(err){
            console.log("Error occurred!", err);
      })
}